import WordCloud from './WordCloud';
import Grafo from './Grafo';
import Map from './Map';
import Treemap from './Treemap';
import Columns from './Columns';

export { WordCloud, Treemap, Map, Grafo, Columns };
