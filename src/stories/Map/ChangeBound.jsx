import React, { Component, Fragment } from 'react';

import Map from '../../lib/Map';

class ChangeBound extends Component {
    constructor(props) {
        super(props);
        this.changeValue = this.changeValue.bind(this);
        this.changeBound = this.changeBound.bind(this);
        this.state = {
            boundingBox: {
                _nw: { lat: 0, lon: 0 },
                _se: { lat: 0, lon: 0 },
            },
            boundMap: null
        };
    }

    changeValue(event) {
        const ids = event.target.id.split('.');
        const { boundingBox } = this.state;
        const newData = boundingBox[ids[0]];
        newData[ids[1]] = parseFloat(event.target.value);
        this.setState({
            boundingBox: {
                ...boundingBox,
                [ids[0]]: { ...newData }
            }
        });
    }

    changeBound() {
        const { _nw, _se } = this.state.boundingBox;
        const newData = [[
            _nw.lon,
            _se.lat
        ], [
            _se.lon,
            _nw.lat
        ]];
        this.setState({
            ...this.state,
            boundMap: newData
        });
    }

    render() {
        const { boundingBox, boundMap } = this.state;
        return (
            <Fragment>
                NW:<br />
                Latitud: <input id="_nw.lat" type="text" onChange={this.changeValue} value={boundingBox._nw.lat} />
                Longitud: <input id="_nw.lon" type="text" onChange={this.changeValue} value={boundingBox._nw.lon} />
                <br /><br />
                SE: <br />
                Latitud: <input id="_se.lat" type="text" onChange={this.changeValue} value={boundingBox._se.lat} />
                Longitud: <input id="_se.lon" type="text" onChange={this.changeValue} value={boundingBox._se.lon} />
                <button onClick={this.changeBound}>Cambiar</button>
                <Map
                    token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                    mapStyle="mapbox://styles/karmaboard/cje4xy8546wbk2so9wkz4hp3b"
                    data={[
                        { key: 'MIKE', position: [19.419968, -99.165], value: 5 },
                        { key: 'DAN', position: [19.419928, -99.164], value: 1 },
                        { key: 'JAVI', position: [19.419468, -99.163], value: 32 },
                        { key: 'JAVI', position: [19.416968, -99.162], value: 1 },
                        { key: 'MIKE', position: [19.411968, -99.161], value: 2 },
                        { key: 'DAN', position: [19.419768, -99.16], value: 1 },
                    ]}
                    colors={[
                        { key: 'MIKE', color: '#FF0000' },
                        { key: 'JAVI', color: '#00FF00' },
                        { key: 'DAN', color: '#0000FF' }
                    ]}
                    boundingBox={boundMap}
                />
            </Fragment>
        );
    }
}

export default ChangeBound;