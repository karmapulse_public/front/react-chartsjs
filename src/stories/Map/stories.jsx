import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import './styles.css';
import { moneyBag, coin } from './customizedIcon';

import Map from '../../lib/Map';
import ChangeBound from './ChangeBound';

import README from './README.md';

storiesOf('Map', module)
    .addWithJSX(
        'Map',
        withNotes(README)(() => (
            <Map
                token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                data={[
                    { key: 'MIKE', position: [19.419968, -99.165], value: 5 },
                    { key: 'DAN', position: [19.419928, -99.164], value: 1 },
                    { key: 'JAVI', position: [19.419468, -99.163], value: 32 },
                    { key: 'JAVI', position: [19.416968, -99.162], value: 1 },
                    { key: 'MIKE', position: [19.411968, -99.161], value: 2 },
                    { key: 'DAN', position: [19.419768, -99.16], value: 1 },
                ]}
                onClickPoint={action('map-point-click')}
                onClickGroup={action('map-group-click')}
                colors={[
                    { key: 'MIKE', color: '#FF0000' },
                    { key: 'JAVI', color: '#00FF00' },
                    { key: 'DAN', color: '#0000FF' }
                ]}
            />
        ))
    )
    .addWithJSX(
        'Set Bounding Box',
        withNotes(README)(() => (
            <Map
                token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                data={[
                    { key: { algo: 'MIKE' }, point: [32.958984, -5.353521], value: 5 },
                    { key: { algo: 'DAN' }, point: [43.50585, 5.615985], value: 1 },
                ]}
                onClickPoint={action('map-point-click')}
                onClickGroup={action('map-group-click')}
                positionName="point"
                colors={[
                    { key: 'MIKE', color: '#FF0000' },
                    { key: 'JAVI', color: '#00FF00' },
                    { key: 'DAN', color: '#0000FF' }
                ]}
                initialBound={[[
                    -105.7032789196819,
                    19.428499988280237
                ], [
                    -99.08313260413706,
                    26.935306442901492
                ]]}
                colorFind={
                    (colors, data, keyName) => (
                        colors.find(item => item[keyName] === data[keyName].algo)
                    )
                }
            />
        ))
    )
    .addWithJSX(
        'Customized Icon',
        withNotes(README)(() => (
            <Map
                token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                data={[
                    { position: [19.419968, -99.165], value: 5 },
                    { position: [19.419928, -99.164], value: 1 },
                    { position: [19.419468, -99.163], value: 32 },
                    { position: [19.416968, -99.162], value: 1 },
                    { position: [19.411968, -99.161], value: 2 },
                    { position: [19.419768, -99.16], value: 1 },
                ]}
                onClickPoint={action('map-point-click')}
                onClickGroup={action('map-group-click')}
                groupIcon={() => moneyBag()}
                singleIcon={() => coin()}
                enableTooltip
            />
        ))
    )
    .addWithJSX(
        'Customized Tooltip',
        withNotes(README)(() => (
            <Map
                token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                data={[
                    { key: 'MIKE', position: [19.419968, -99.165], value: 5 },
                    { key: 'DAN', position: [19.419928, -99.164], value: 1 },
                    { key: 'JAVI', position: [19.419468, -99.163], value: 32 },
                    { key: 'JAVI', position: [19.416968, -99.162], value: 1 },
                    { key: 'MIKE', position: [19.411968, -99.161], value: 2 },
                    { key: 'DAN', position: [19.419768, -99.16], value: 1 },
                ]}
                onClickPoint={action('map-point-click')}
                onClickGroup={action('map-group-click')}
                groupIcon={() => moneyBag()}
                singleIcon={() => coin()}
                enableTooltip
                tooltip={
                    data => (
                        <div className="tooltip-example">
                            <h1>{data.key}</h1>
                            <h3>{data.value}</h3>
                        </div>
                    )
                }
            />
        ))
    )
    .addWithJSX(
        'Customized Background',
        withNotes(README)(() => (
            <Map
                token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                mapStyle="mapbox://styles/karmaboard/cje4xy8546wbk2so9wkz4hp3b"
                data={[
                    { key: 'MIKE', position: [19.419968, -99.165], value: 5 },
                    { key: 'DAN', position: [19.419928, -99.164], value: 1 },
                    { key: 'JAVI', position: [19.419468, -99.163], value: 32 },
                    { key: 'JAVI', position: [19.416968, -99.162], value: 1 },
                    { key: 'MIKE', position: [19.411968, -99.161], value: 2 },
                    { key: 'DAN', position: [19.419768, -99.16], value: 1 },
                ]}
                onClickPoint={action('map-point-click')}
                onClickGroup={action('map-group-click')}
                colors={[
                    { key: 'MIKE', color: '#FF0000' },
                    { key: 'JAVI', color: '#00FF00' },
                    { key: 'DAN', color: '#0000FF' }
                ]}
            />
        ))
    )
    .addWithJSX(
        'Change Bounding Box',
        withNotes(README)(() => (
            <ChangeBound />
        ))
    );
