Columnas
===================

## Props

** series ** _(Array)_

La información debe ser un arreglo de objetos que tenga la siguiente estructura:
```
[{
    name: 'PRODUCT A',
    data: [
        { x: '01/01/2011', y: 4 },
        { x: '01/02/2011', y: 55 },
        { x: '01/03/2011', y: 41 },
        { x: '01/04/2011', y: 67 },
        { x: '01/05/2011', y: 22 },
        { x: '01/06/2011', y: 43 }
    ]
}]
```
Cada objeto dentro del arreglo se le asignará un color específico y contiene los siguientes valores:
* _name_: será el nombre del bucket.
* _data_: será otro arreglo de objetos donde cada objeto tendrá dos campos: 'x' y 'y' refiriendose a los ejes correspondientes que contiene la gráfica.

** onClick ** _(Func)_

Función que se obtiene al darle click a un recuadro específico de alguna columna. El valor devuelto será un objeto con dos valores dentro:
* _seriesIndex_: es el índice del arreglo 'series' (descrito anteriormente)
* _dataIndex_: es el índice del arreglo 'data' que viene dentro del objeto seleccionado dentro de 'series'.

** locale ** _(String)_

Sirve para cambiar el idioma de los tooltips del toolbar y el de las fechas. Solo existen dos valores por el momento: 'es' (default) y 'en'.

** xType ** _(String)_

Para pintar bien el eje X se debe colocar el valor correspondiente según lo que estés asignando en el campo 'x' del arreglo 'data' dentro de 'series'. Existen tres valores:
* _datetime_: (default) Para pintar fechas correctamente.
* _category_: Para pintar categorias. Ej. Frutas, Verduras, Animales, etc.
* _numeric_: Para pintar solo números.

** showToolbar ** _(Bool)_

Sirve para ocultar o mostrar el toolbar que se encuentra arriba de la gráfica.

** config ** _(Objeto)_

Si deseas agregar configuraciones extras a esta gráfica, se debe pasar un objeto con cualquiera de las especificaciones que se encuentran en la documentación de ApexCharts: [Opciones](https://apexcharts.com/docs/options/).

## Proximamente

** toolbarIcons **

----------
