import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import Columns from '../../lib/Columns';

import README from './README.md';

const data = [{
    name: 'PRODUCT A',
    data: [
        { x: '01/01/2011', y: 4 },
        { x: '01/02/2011', y: 55 },
        { x: '01/03/2011', y: 41 },
        { x: '01/04/2011', y: 67 },
        { x: '01/05/2011', y: 22 },
        { x: '01/06/2011', y: 43 }
    ]
}, {
    name: 'PRODUCT B',
    data: [
        { x: '01/01/2011', y: 3 },
        { x: '01/02/2011', y: 23 },
        { x: '01/03/2011', y: 20 },
        { x: '01/04/2011', y: 8 },
        { x: '01/05/2011', y: 13 },
        { x: '01/06/2011', y: 27 }
    ]
}, {
    name: 'PRODUCT C',
    data: [
        { x: '01/01/2011', y: 1 },
        { x: '01/02/2011', y: 17 },
        { x: '01/03/2011', y: 15 },
        { x: '01/04/2011', y: 15 },
        { x: '01/05/2011', y: 21 },
        { x: '01/06/2011', y: 14 }
    ]
}, {
    name: 'PRODUCT D',
    data: [
        { x: '01/01/2011', y: 2 },
        { x: '01/02/2011', y: 7 },
        { x: '01/03/2011', y: 25 },
        { x: '01/04/2011', y: 13 },
        { x: '01/05/2011', y: 22 },
        { x: '01/06/2011', y: 8 }
    ]
}];

storiesOf('Bars', module)
    .addWithJSX(
        'Stacked Columns',
        withNotes(README)(() => (
            <div style={{ width: 600, height: 350 }}>
                <Columns series={data} onClick={action('Columns-click')} />
            </div>
        ))
    );
