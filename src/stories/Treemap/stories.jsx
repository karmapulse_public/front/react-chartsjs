import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import random from 'lodash/random';
import range from 'lodash/range';
import Treemap from '../../lib/Treemap/Treemap';

import README from './README.md';

import randomWords from 'random-words';

const colors = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"];
const data = range(3).map(() => ({
    name: randomWords(),
    color: colors[random(0, 9)],
    children: randomWords(random(1, 10)).map(() => ({
        name: randomWords(),
        total: random(1, 1000)
    }))
}));
const datum = [
	{
		name: 'algo',
		children: [
			{
				name: 'otro',
				total: 1
			},
			{
				name: 'qwe',
				total: 2
			},
			{
				name: 'fdsg',
				total: 3
			},
			{
				name: 'tre',
				total: 4
			},
			{
				name: 'fds',
				total: 5
			},
		],
		color: '#F00'
	},
	{
		name: 'venegas',
		children: [
			{
				name: 'otro',
				total: 112
			},
			{
				name: 'qwe',
				total: 232
			},
			{
				name: 'fdsg',
				total: 343
			},
			{
				name: 'tre',
				total: 454
			},
			{
				name: 'fds',
				total: 565
			},
		],
		color: '#0F0'
	},
];
storiesOf('Treemap', module)
    .addWithJSX(
        'Default',
        withNotes(README)(() => (
            <Treemap // eslint-disable-line
                data={data}
                dataKey="total"
                onDataClick={action('Treemap-click')}
            />
        ))
    )
    .addWithJSX(
        'Tooltip',
        withNotes(README)(() => (
            <Treemap // eslint-disable-line
                data={datum}
                dataKey="total"
                onDataClick={action('Treemap-click')}
                enableTooltip
            />
        ))
    )
    .addWithJSX(
        'Custom Tooltip',
        withNotes(README)(() => (
            <Treemap // eslint-disable-line
                data={data}
                dataKey="total"
                onDataClick={action('Treemap-click')}
                enableTooltip
                tooltip={
                    (props) => {
                        const payload = (props.payload.length >= 1) ? props.payload[0].payload : { name: '', value: '' };
                        return (
                            <div className="tooltip-example">
                                <p>{payload.name}</p>
                                <p>{payload.value}</p>
                            </div>
                        );
                    }
                }
            />
        ))
    );
