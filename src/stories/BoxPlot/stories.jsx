import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import { D3 } from '../../lib/BoxPlot/defaultValues';
import BoxPlot from '../../lib/BoxPlot';

import README from './README.md';

// Generate five 100 count, normal distributions with random means
const groupCounts = {};
const meanGenerator = D3.randomUniform(1, 10);
for (let i = 0; i < 10; i += 1) {
    const randomMean = meanGenerator();
    const generator = D3.randomNormal(randomMean);
    const key = `AA-${i}`;
    groupCounts[key] = [];

    for (let j = 0; j < 10; j += 1) {
        const entry = generator();
        groupCounts[key].push(entry);
    }
}

storiesOf('BoxPlot', module)
    .addWithJSX(
        'default',
        withNotes(README)(() => (
            <BoxPlot
                data={groupCounts}
                onHover={action('boxplot-hover')}
                onClick={action('boxplot-click')}
            />
        ))
    );
