import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import Grafo from '../../lib/Grafo';
import README from './README.md';

const words = {
    links: [
        {
            source: 'Monitoreo_Flc',
            target: 'Seguros_Atlas'
        },
        {
            source: 'PSK_seguridad',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'MonitorTIUSA',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'TabascoHOY',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'monitoreotrec2',
            target: 'Monitoreo_Flc'
        },
        {
            source: '5sobreruedas',
            target: 'Monitoreo_Flc'
        },
        {
            source: '5sobreru',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'SCJN',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'aespindolac',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'lopezobrador_',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'trailerosypocas',
            target: 'Monitoreo_Flc'
        },
        {
            source: 'AntonMge',
            target: 'Seguros_Atlas'
        },
        {
            source: '_Angustina_',
            target: 'AntonMge'
        },
        {
            source: 'AnaSBarrera24',
            target: 'AntonMge'
        },
        {
            source: 'MarAbiertoBR',
            target: 'AntonMge'
        },
        {
            source: 'IxZihuatanejo',
            target: 'AntonMge'
        },
        {
            source: 'Jvsticivs',
            target: 'AntonMge'
        },
        {
            source: 'duriva',
            target: 'AntonMge'
        },
        {
            source: 'talent_net',
            target: 'AntonMge'
        },
        {
            source: 'ANewsMX',
            target: 'AntonMge'
        },
        {
            source: 'Siuatsintli',
            target: 'AntonMge'
        },
        {
            source: 'Arq_AlejandroG',
            target: 'Seguros_Atlas'
        },
        {
            source: 'AOC',
            target: 'Arq_AlejandroG'
        },
        {
            source: 'F1',
            target: 'Arq_AlejandroG'
        },
        {
            source: 'noemirodvel',
            target: 'Seguros_Atlas'
        },
        {
            source: 'marcosbaghdatis',
            target: 'noemirodvel'
        },
        {
            source: 'NickKyrgios',
            target: 'noemirodvel'
        },
        {
            source: 'philiptheprod',
            target: 'noemirodvel'
        },
        {
            source: 'ellsbells89',
            target: 'noemirodvel'
        },
        {
            source: 'CocoGauff',
            target: 'noemirodvel'
        },
        {
            source: 'AmbPierreAlarie',
            target: 'noemirodvel'
        },
        {
            source: 'JohnIsner',
            target: 'noemirodvel'
        },
        {
            source: 'SoyCelestee',
            target: 'noemirodvel'
        },
        {
            source: 'DeniseWilkes12',
            target: 'Seguros_Atlas'
        },
        {
            source: 'leinrockuptible',
            target: 'MetLifeMx'
        },
        {
            source: 'da_ggb',
            target: 'leinrockuptible'
        },
        {
            source: 'DIvan_Gzz',
            target: 'leinrockuptible'
        },
        {
            source: 'Alexand00668244',
            target: 'leinrockuptible'
        },
        {
            source: 'Pako_bang',
            target: 'leinrockuptible'
        },
        {
            source: 'wl4gner',
            target: 'leinrockuptible'
        },
        {
            source: 'Psycub',
            target: 'leinrockuptible'
        },
        {
            source: 'MarioCisneros21',
            target: 'leinrockuptible'
        },
        {
            source: 'erick_pachon',
            target: 'leinrockuptible'
        },
        {
            source: 'jashez_',
            target: 'leinrockuptible'
        },
        {
            source: 'JessGalCo',
            target: 'MetLifeMx'
        },
        {
            source: 'poemaniacos',
            target: 'JessGalCo'
        },
        {
            source: 'pdepoesia',
            target: 'JessGalCo'
        },
        {
            source: 'lapoesiamex',
            target: 'JessGalCo'
        },
        {
            source: 'PoesiaNomada',
            target: 'JessGalCo'
        },
        {
            source: 'circulodepoesia',
            target: 'JessGalCo'
        },
        {
            source: 'RMartinezGuzman',
            target: 'JessGalCo'
        },
        {
            source: 'PoesiaYRelatos',
            target: 'JessGalCo'
        },
    ],
    nodes: [
        {
            group: 'Otro',
            id: 'otro',
            type: 'account',
            value: 78
        },
        {
            group: 'Seguros_Atlas',
            id: 'Seguros_Atlas',
            type: 'account',
            value: 10
        },
        {
            group: 'Seguros_Atlas',
            id: 'Monitoreo_Flc',
            type: 'follower_primary',
            value: 8
        },
        {
            group: 'none',
            id: 'PSK_seguridad',
            type: 'follower_secondary',
            value: 84
        },
        {
            group: 'none',
            id: 'MonitorTIUSA',
            type: 'follower_secondary',
            value: 7
        },
        {
            group: 'none',
            id: 'TabascoHOY',
            type: 'follower_secondary',
            value: 92
        },
        {
            group: 'none',
            id: 'monitoreotrec2',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: '5sobreruedas',
            type: 'follower_secondary',
            value: 53
        },
        {
            group: 'none',
            id: '5sobreru',
            type: 'follower_secondary',
            value: 74
        },
        {
            group: 'none',
            id: 'SCJN',
            type: 'follower_secondary',
            value: 80
        },
        {
            group: 'none',
            id: 'aespindolac',
            type: 'follower_secondary',
            value: 90
        },
        {
            group: 'none',
            id: 'lopezobrador_',
            type: 'follower_secondary',
            value: 22
        },
        {
            group: 'none',
            id: 'trailerosypocas',
            type: 'follower_secondary',
            value: 55
        },
        {
            group: 'Seguros_Atlas',
            id: 'AntonMge',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'none',
            id: '_Angustina_',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'AnaSBarrera24',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'MarAbiertoBR',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'IxZihuatanejo',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'Jvsticivs',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'duriva',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'talent_net',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'ANewsMX',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'Siuatsintli',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'Seguros_Atlas',
            id: 'Arq_AlejandroG',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'none',
            id: 'AOC',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'F1',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'Seguros_Atlas',
            id: 'noemirodvel',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'none',
            id: 'marcosbaghdatis',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'NickKyrgios',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'philiptheprod',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'ellsbells89',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'CocoGauff',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'AmbPierreAlarie',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'JohnIsner',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'SoyCelestee',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'Seguros_Atlas',
            id: 'DeniseWilkes12',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'MetLifeMx',
            id: 'MetLifeMx',
            type: 'account',
            value: 14
        },
        {
            group: 'MetLifeMx',
            id: 'leinrockuptible',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'none',
            id: 'da_ggb',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'DIvan_Gzz',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'Alexand00668244',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'Pako_bang',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'wl4gner',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'Psycub',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'MarioCisneros21',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'erick_pachon',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'jashez_',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'MetLifeMx',
            id: 'JessGalCo',
            type: 'follower_primary',
            value: 14
        },
        {
            group: 'none',
            id: 'poemaniacos',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'pdepoesia',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'lapoesiamex',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'PoesiaNomada',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'circulodepoesia',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'RMartinezGuzman',
            type: 'follower_secondary',
            value: 14
        },
        {
            group: 'none',
            id: 'PoesiaYRelatos',
            type: 'follower_secondary',
            value: 14
        },
    ],
};

const colors = {
    Seguros_Atlas: 'rgb(44, 160, 44)',
    MetLifeMx: 'rgb(23, 190, 207)',
    AXAMexico: 'rgb(31, 119, 180)',
    MAPFRE_MX: 'rgb(214, 39, 40)',
    none: 'rgb(127, 127, 127)',
};

const sizes = {
    account: 20,
    follower_primary: 10,
    follower_secondary: 3
};


storiesOf('Grafo', module)
    .addWithJSX(
        'Default',
        withNotes(README)(() => (
            <Grafo  // eslint-disable-line
                data={words}
                onWordClick={action('wordcloud-click')}
                colors={{}}
                colorDefault="#F00"
                minSize={5}
                maxSize={100}
                linkColor="#F00"
                nodeBorderColor="#0F0"
            />
        ))
    )
    .addWithJSX(
        'tooltip',
        withNotes(README)(() => (
            <Grafo  // eslint-disable-line
                data={words}
                height={500}
                width={500}
                onWordClick={action('wordcloud-click')}
                colors={colors}
                sizes={sizes}
                linkColor="#F00"
                nodeBorderColor="#0F0"
                enableTooltip
            />
        ))
    )
    .addWithJSX(
        'custom tooltip',
        withNotes(README)(() => (
            <Grafo  // eslint-disable-line
                data={words}
                height={500}
                width={500}
                onWordClick={action('wordcloud-click')}
                colors={colors}
                sizes={sizes}
                linkColor="#F00"
                nodeBorderColor="#0F0"
                enableTooltip
                tooltip={
                    data => (
                        <div style={{ border: '1px solid red' }}>
                            <p>{data.id}</p>
                            <p>{data.value}</p>
                        </div>
                    )
                }
            />
        ))
    );
