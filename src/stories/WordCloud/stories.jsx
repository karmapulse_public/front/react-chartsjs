import React from 'react';
import range from 'lodash/range';
import randomWords from 'random-words';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';

import WordCloud from '../../lib/WordCloud';

import README from './README.md';

const words = range(50).map(() => ({
    name: randomWords(),
    total: Math.round(Math.random() * 1000)
}));

storiesOf('WordCloud', module)
    .addWithJSX(
        'Default',
        withNotes(README)(() => (
            <WordCloud  // eslint-disable-line
                words={words}
                wordCountKey="total"
                wordKey="name"
                height={500}
                width={500}
                onWordClick={action('wordcloud-click')}
            />
        ))
    )
    .addWithJSX(
        'Rotate',
        withNotes(README)(() => (
            <WordCloud
                words={words}
                wordCountKey="total"
                wordKey="name"
                height={500}
                width={500}
                maxAngle={60}
                minAngle={-60}
                orientations={15}
                onWordClick={action('wordcloud-click')}
            />
        ))
    )
    .addWithJSX(
        'Tooptip',
        withNotes(README)(() => (
            <WordCloud
                words={words}
                wordCountKey="total"
                wordKey="name"
                height={500}
                width={500}
                enableTooltip
                onWordClick={action('wordcloud-click')}
            />
        ))
    )
    .addWithJSX(
        'Customized Tooptip',
        withNotes(README)(() => (
            <WordCloud
                words={words}
                wordCountKey="total"
                wordKey="name"
                height={500}
                width={500}
                enableTooltip
                tooltip={
                    data => (
                        <div className="tooltip-example">
                            <p>{data.text}</p>
                            <p>{data.total}</p>
                        </div>
                    )
                }
                onWordClick={action('wordcloud-click')}
            />
        ))
    );
