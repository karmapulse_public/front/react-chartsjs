import WordCloud from './WordCloud';
import Grafo from './Grafo';
import Map from './Map';
import Treemap from './Treemap';

export { WordCloud, Treemap, Map, Grafo };
