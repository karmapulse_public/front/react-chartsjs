import React from 'react';
import PropTypes from 'prop-types';
import ReactApexChart from 'react-apexcharts';

const Columns = ({
    series, onClick, locale, xType, config, showToolbar
}) => {
    const options = {
        chart: {
            stacked: true,
            toolbar: {
                show: showToolbar,
                tools: {
                    selection: true,
                    zoom: false
                }
            },
            defaultLocale: locale,
            locales: [{
                name: 'en',
                options: {
                    months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                    shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
                    toolbar: {
                        exportToSVG: 'Download SVG',
                        exportToPNG: 'Download PNG',
                        selection: 'Selection',
                        selectionZoom: 'Selection Zoom',
                        zoomIn: 'Zoom In',
                        zoomOut: 'Zoom Out',
                        pan: 'Panning',
                        reset: 'Reset Zoom',
                    }
                }
            },
            {
                name: 'es',
                options: {
                    months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                    shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                    days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                    shortDays: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                    toolbar: {
                        exportToSVG: 'Descarga SVG',
                        exportToPNG: 'Descarga PNG',
                        selection: 'Selección',
                        selectionZoom: 'Selección Zoom',
                        zoomIn: 'Más cerca',
                        zoomOut: 'Más lejos',
                        pan: 'Mover',
                        reset: 'Limpiar zoom',
                    }
                }
            }],
            events: {
                dataPointSelection: (event, chartContext, conf) => {
                    onClick({
                        dataIndex: conf.seriesIndex,
                        seriesIndex: conf.dataPointIndex
                    });
                }
            }
        },
        plotOptions: {
            bar: {
                horizontal: false
            }
        },
        xaxis: {
            type: xType,
        },
        dataLabels: {
            enabled: false
        },
        fill: {
            opacity: 1
        },
        ...config
    };
    return (
        <ReactApexChart
            type="bar"
            series={series}
            options={options}
        />
    );
};

Columns.propTypes = {
    xType: PropTypes.string,
    onClick: PropTypes.func,
    locale: PropTypes.string,
    config: PropTypes.shape({}),
    showToolbar: PropTypes.bool,
    series: PropTypes.arrayOf(PropTypes.shape({})),
};

Columns.defaultProps = {
    series: [],
    config: {},
    locale: 'es',
    xType: 'datetime',
    showToolbar: true,
    onClick: () => ({}),
};

export default Columns;
