import PropTypes from 'prop-types';

export const MIN_HEIGHT = 200;
export const MIN_WIDTH = 200;
export const DEFAULT_COLOR = '#000';
export const DEFAULT_LINK_COLOR = '#000';
export const DEFAULT_BORDER_COLOR = '#FFF';
export const MAX_SIZE = 20;
export const MIN_SIZE = 10;

export const propTypes = {
    height: PropTypes.number,
    width: PropTypes.number,
    colors: PropTypes.object,
    colorDefault: PropTypes.string,
    linkColor: PropTypes.string,
    nodeBorderColor: PropTypes.string,
    sizes: PropTypes.object,
    enableTooltip: PropTypes.bool,
    tooltip: PropTypes.func,
    onNodeClick: PropTypes.func,
    maxDiameter: PropTypes.number,
    minDiameter: PropTypes.number,
    collisionDistance: PropTypes.number,
    atractionStrength: PropTypes.number,
    withArrowHead: PropTypes.bool,
    straightLink: PropTypes.bool,
};

export const defaultProps = {
    height: null,
    width: null,
    colors: {},
    colorDefault: DEFAULT_COLOR,
    linkColor: DEFAULT_LINK_COLOR,
    nodeBorderColor: DEFAULT_BORDER_COLOR,
    sizes: {},
    enableTooltip: true,
    onNodeClick: () => ({}),
    maxDiameter: MAX_SIZE,
    minDiameter: MIN_SIZE,
    collisionDistance: 10,
    atractionStrength: -1,
    withArrowHead: false,
    straightLink: false
};
