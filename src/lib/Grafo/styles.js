import css from 'glamor-jss';

export const styles = () => css({
    '&.react-charts-grafo': {
        width: '100%',
        height: '100%',
        '& .react-charts-grafo__chart': {
            width: '100%',
            height: '100%',
        }

    },
});

export default styles;
