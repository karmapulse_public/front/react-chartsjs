import React from 'react';
import d3Grafos from './classD3';
import Tooltip from '../Tooltip/Tooltip';

import {
    propTypes,
    defaultProps
} from './defaultValues';

import styles from './styles';

class Grafo extends React.Component {
    container = React.createRef(); // eslint-disable-line
    chart = React.createRef(); // eslint-disable-line

    state = {
        tooltipContent: '',
        showTooltip: false,
        tooltipX: 0,
        tooltipY: 0,
    }

    componentDidMount() {
        // this.validateProps();
        d3Grafos.create(
            this.props,
            Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            })
        );
    }
    tooltipState = (arrs) => {
        this.setState(arrs);
    }
    componentWillReceiveProps(nextProps) {
        d3Grafos.update(
            nextProps,
            Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            })
        );
    }

    componentWillUnmount() {
        d3Grafos.destroy(this.chart.current);
    }

    render() {
        const {
            tooltipContent,
            showTooltip,
            tooltipX,
            tooltipY,
        } = this.state;
        const { tooltip } = this.props;
        return (
            <div className="react-charts-grafo" ref={this.container} {...styles()}>
                <div className="react-charts-grafo__chart" ref={this.chart} />
                <Tooltip x={tooltipX} y={tooltipY} isEnabled={showTooltip}>
                    {
                        tooltip !== undefined ? tooltip(tooltipContent) : <h1>{tooltipContent}</h1>
                    }
                </Tooltip>
            </div>
        );
    }
}

Grafo.propTypes = propTypes;
Grafo.defaultProps = defaultProps;

export default Grafo;
