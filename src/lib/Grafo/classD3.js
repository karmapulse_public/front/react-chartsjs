
import * as D3 from 'd3';

import { MIN_HEIGHT, MIN_WIDTH, DEFAULT_COLOR } from './defaultValues';

export default (() => {
    let _svg;
    let _width;
    let _height;
    let _maxValue;
    let _maxDiameter;
    let _minDiameter;


    let props = {};
    let config = {};

    const setDimensions = (height, width) => {
        const {
            chart
        } = config;
        const {
            offsetHeight: parentHeight,
            offsetWidth: parentWidth
        } = chart.parentNode;
        // console.log(parentHeight);
        _height = height || parentHeight;
        _width = width || parentWidth;

        if (typeof _height !== 'number' || _height < MIN_HEIGHT) {
            console.warn(`Invalid/small height provided, falling back to minimum value of ${MIN_HEIGHT}`);
            _height = MIN_HEIGHT;
        }

        if (typeof _width !== 'number' || _width < MIN_WIDTH) {
            console.warn(`Invalid/small width provided, falling back to minimum value of ${MIN_HEIGHT}`);
            _width = MIN_HEIGHT;
        }
    };
    /*
    **  validateStrength sirve para validar el valor dado
    **  por el usuario, solo se aceptan valores del 0 a 1
    */
    const validateStrength = strength => strength >= 0 && strength <= 1;

    /*
    **  blockXEdge sirve para bloquear los bordes de la gráfica
    **  hace la validación y calculos para que el nodo no salga de los bordes de
    **  la gráfica y al final retorna la nueva posición en X.
    */
    const blockXEdge = (nodes, index, x) => {
        const r = parseInt(D3.select(nodes[index]).attr('r'), 10);
        let finalX = x;
        //  Se bloquean los bordes.
        if (finalX + r > _width) finalX = _width - r;
        if (finalX - r < 0) finalX = 0 + r;

        return finalX;
    };

    /*
    **  blockYEdge sirve para bloquear los bordes de la gráfica
    **  hace la validación y calculos para que el nodo no salga de los bordes de
    **  la gráfica y al final retorna la nueva posición en Y.
    */
    const blockYEdge = (nodes, index, y) => {
        const r = parseInt(D3.select(nodes[index]).attr('r'), 10);
        let finalY = y;
        //  Se bloquean los bordes.
        if (finalY + r > _height) finalY = _height - r;
        if (finalY - r < 0) finalY = 0 + r;

        return finalY;
    };

    /*
    **  setMaxValue sirve para obtener el valor más grande
    **  de todos los nodos que se usan para crear el nodo. Y con ello poder
    **  hacer un calculo automatico de los tamaños, en caso de no mandar tamaños.
    */
    const setMaxValue = (data) => {
        const values = data.nodes.map(o => o.value);
        _maxValue = values.reduce((ant, act) => { if (ant > act) return ant; return act; });
    };

    /*
    **  setSizes sirve para obtener de radio para cada nodo
    **  con respecto a un diametro máximo y otro mínimo.
    **  Se utiliza si no se ponen los tamaños desde fuera.
    */
    const setSizes = (d, sizes) => {
        const existType = Object.keys(sizes).includes(d.type);
        if (existType) {
            return sizes[d.type];
        }

        return ((d.value * (_maxDiameter / 2)) / _maxValue) + (_minDiameter / 2);
    };

    /*
    **  setColors sirve para mapear los colores a los nodos
    **  En caso de no tener arreglo de colores se coloca un por
    **  defecto.
    */
    const setColors = (d, colors, colorDefault) => {
        const existGroup = Object.keys(colors).includes(d.group.toString());
        if (existGroup) {
            return colors[d.group];
        }

        return colorDefault;
    };

    /*
    **  setCollisionDistance setea la distancia de separación
    **  o colision entre nodos, la distancia en la que ya se mueven los nodos
    **  al tocar a otros. Se pone de base el radio de los nodos
    **  más lo que el usuario setea.
    */
    const setCollisionDistance = (d, listNodes, collisionDistance) => (
        collisionDistance + parseInt(D3.select(listNodes._groups[0][d.index]).attr('r'), 10)
    );

    const drag = (simulation) => {
        const dragstarted = (e) => {
            if (!D3.event.active) simulation.alphaTarget(0.3).restart();
            e.fx = e.x;
            e.fy = e.y;
        };
        const dragged = (e, i, nodes) => {
            e.fx = blockXEdge(nodes, i, D3.event.x);
            e.fy = blockYEdge(nodes, i, D3.event.y);
        };
        const dragended = (e) => {
            if (!D3.event.active) simulation.alphaTarget(0);
            e.fx = null;
            e.fy = null;
        };
        return D3
            .drag()
            .on('start', dragstarted)
            .on('drag', dragged)
            .on('end', dragended);
    };

    const update = (newProps, newConfig) => {
        props = newProps;
        config = newConfig;
        const {
            height,
            width,
            data,
            colors,
            colorDefault,
            linkColor,
            nodeBorderColor,
            sizes,
            onNodeClick,
            maxDiameter,
            minDiameter,
            collisionDistance,
            atractionStrength,
            withArrowHead,
            straightLink
        } = props;
        /*
        **  Array que almacena los diferentes tamaños de radios de los nodos,
        **  para poder crear las diferentes cabezas de flechas con la  distancia
        **  correcta.
        */
        const sizeForArrow = [];
        if (!data.links || data.links.length === 0 || !data.nodes || data.nodes.length === 0) {
            return;
        }
        setDimensions(height, width);
        setMaxValue(data);
        _maxDiameter = maxDiameter;
        _minDiameter = minDiameter;

        _svg
            .attrs({
                height: _height,
                width: _width
            });

        const links = data.links.map(d => Object.create(d));
        const nodes = data.nodes.map(d => Object.create(d));

        const simulation = D3
            .forceSimulation(nodes);
        /*
        **  Se crean los links entre nodos
        */
        const link = _svg
            .append('svg:g')
            .selectAll('path')
            .data(links)
            .enter()
            .append('svg:path')
            .attr('class', 'graph-link')
            .attr('fill', 'none')
            .attr('stroke-opacity', 0.6)
            .attr('stroke-width', 1.5)
            .attr('stroke', linkColor)
            .style('stroke-width', 2);
        /*
        **  Se crean los nodos
        */
        const node = _svg
            .append('g')
            .attr('stroke', nodeBorderColor)
            .attr('stroke-width', 1.5)
            .selectAll('circle')
            .data(nodes)
            .join('circle')
            .attr('r', (d) => {
                const size = setSizes(d, sizes);
                if (!sizeForArrow.includes(size)) sizeForArrow.push(size);
                return size;
            })
            .attr('fill', d => setColors(d, colors, colorDefault))
            .attr('cursor', 'pointer')
            .call(drag(simulation))
            .on('click', onNodeClick)
            /*
            **  Evento utilizado para animar los nodos al tocar
            **  un nodo y mostrar el tooltip
            */
            .on('mouseenter', (event, index, parent) => {
                const actualNode = D3.select(parent[index]);
                node
                    .transition()
                    .style('opacity', 0.5);
                actualNode
                    .transition()
                    .style('opacity', 1);
                const { enableTooltip, tooltip } = props;
                const tooltipContent = tooltip ?
                    event : `${event.id}`;
                if (enableTooltip) {
                    config.tooltipState({
                        tooltipContent,
                        showTooltip: true,
                        tooltipX: D3.event.pageX,
                        tooltipY: D3.event.pageY - 28,

                    });
                }
            })
            /*
            **  Evento utilizado para animar los nodos al dejar de tocar
            **  un nodo y desaparecer el tooltip
            */
            .on('mouseout', (event, index, parent) => {
                const actualNode = D3.select(parent[index]);
                node
                    .transition()
                    .style('opacity', 1);

                actualNode
                    .transition()
                    .style('opacity', 1);
                if (props.enableTooltip) {
                    config.tooltipState({
                        showTooltip: false,
                    });
                }
            });
        /*
        **  Defs es la variable que se utiliza para
        **  agregarle las diferentes flechas que se utilizan
        **  en los links
        */
        const defs = _svg
            .append('svg:defs')
            .selectAll('marker')
            .data(['end'])
            .enter();

        const forceLink = D3.forceLink(links).id(d => d.id);
        const forceCollide = D3.forceCollide().radius(d => setCollisionDistance(d, node, collisionDistance));
        const forceCenter = D3.forceCenter(_width / 2, _height / 2);
        if (validateStrength(atractionStrength)) {
            forceLink.strength(atractionStrength);
        }
        simulation
            .force('link', forceLink)
            .force('collision', forceCollide)
            .force('center', forceCenter);

        if (withArrowHead) {
            /*
            **  Se crea cada una de las cabezas de flecha dependiendo del
            **  tamaño de los radios de los nodos. Agregandole el id
            **  'arrow' + radio -> arrow5
            */
            sizeForArrow.forEach((size) => {
                defs
                    .append('svg:marker')
                    .attr('id', `arrow${size}`)
                    .attr('viewBox', '0 -5 10 10')
                    .attr('refX', size + 7)
                    .attr('markerWidth', 6)
                    .attr('markerHeight', 6)
                    .attr('orient', 'auto')
                    .append('svg:path')
                    .attr('d', 'M0,-5L10,0L0,5')
                    .attr('fill', linkColor);
            });
        }

        simulation.on('tick', () => {
            link
                .attr('d', (d) => {
                    const dx = d.target.x - d.source.x;
                    const dy = d.target.y - d.source.y;
                    const r = parseInt(D3.select(node._groups[0][d.index]).attr('r'), 10);
                    let finalX = d.x;
                    let finalY = d.y;
                    //  Se bloquean los bordes.
                    if (finalX + r > _width) finalX = _width - r;
                    if (finalY + r > _height) finalY = _height - r;
                    if (finalX - r < 0) finalX = 0 + r;
                    if (finalY - r < 0) finalY = 0 + r;
                    const dr = Math.sqrt((dx * dx) + (dy * dy));
                    if (straightLink) {
                        return `M${
                            d.source.x},${
                            d.source.y}L${
                            d.target.x} ${
                            d.target.y}`;
                    }
                    return `M${
                        d.source.x},${
                        d.source.y}A${
                        dr},${dr} 0 0,1 ${
                        d.target.x} ${
                        d.target.y}`;
                });
            //  Se hace un link la flecha correspondiente, depende de el radio del nodo destino.
            link
                .attr('marker-end', d => `url(#arrow${D3.select(node._groups[0][d.target.index]).attr('r')})`);
            node
                .attr('transform', d => (
                    `translate(${blockXEdge(node._groups[0], d.index, d.x)},${blockYEdge(node._groups[0], d.index, d.y)})`
                ));
        });
    };

    const create = (outProps, outConfig) => {
        props = outProps;
        config = outConfig;
        const { chart } = config;

        D3
            .select(chart)
            .selectAll('*')
            .remove();

        _svg = D3.select(chart).append('svg');
        update(props, config);
    };

    const destroy = (chart) => {
        D3.select(chart).selectAll('*').remove();
        _svg.remove();
    };

    return {
        create,
        update,
        destroy
    };
})();
