import React from 'react';
import { Treemap as TreemapRe, Tooltip } from 'recharts';
import CustomizedContent from './CustomizedContent';
import {
    propTypes,
    defaultProps
} from './defaultValues';

const NewToolTip = (props) => {
    const payload = (props.payload.length >= 1) ? props.payload[0].payload : { name: '', value: '' };
    return (
        <div className="react-chartsjs__tooltip" style={{ position: 'relative' }}>
            {payload.name} : {payload.value}
        </div>
    );
};

const Treemap = props => (
    <TreemapRe
        width={props.width}
        height={props.height}
        data={props.data}
        dataKey={props.dataKey}
        ratio={16 / 9}
        stroke={props.stroke}
        fill={props.fill}
        fontFamily={props.fontFamily}
        fontSize={props.fontSize}
        content={
            <CustomizedContent
                colors={props.colors}
                onDataClick={props.onDataClick}
            />}
    >
        {
            props.enableTooltip
                ? <Tooltip content={
                    props.tooltip ? props.tooltip : <NewToolTip />
                }
                />
                : null
        }
    </TreemapRe>
);

Treemap.propTypes = propTypes;
Treemap.defaultProps = defaultProps;

export default Treemap;
