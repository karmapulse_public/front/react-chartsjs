import React from 'react';

/* eslint no-mixed-operators:
    ["error", {"groups": [["&", "|", "^", "~", "<<", ">>", ">>>"], ["&&", "||"]]}]
*/

const CustomizedContent = (props) => {
    const {
        root, depth, x, y, width, height, index, payload, colors, rank, name, onDataClick
    } = props;
    let heightValue
    const color = (props.depth === 2) ? props.root.color : null;
    return (
        <g onClick={() => onDataClick(props)}>
            <rect
                x={x}
                y={y}
                width={width}
                height={height}
                style={{
                    fill: color,
                    stroke: '#fff',
                    strokeWidth: 1.5
                    // strokeWidth: 2 / (depth + 1e-10),
                    // strokeOpacity: 1 / (depth + 1e-10),
                }}
            />
            {
                (() => {
                    if (depth !== 1) {
                        if (width > 80 && height > 30) {
                            return (
                                <text
                                    x={x + 15}
                                    y={y + 30}
                                    stroke="none"
                                    textAnchor="start"
                                >
                                    {name}
                                </text>
                            );
                        }
                        return null;
                    }
                    return null;
                })()

            }
            {
                // depth === 1 ?
                //     <text
                //         x={x + width / 2}
                //         y={y + height / 2 + 7}
                //         textAnchor="middle"
                //         fill="#fff"
                //         fontSize={14}
                //     >
                //         {name}
                //     </text>
                //     : null
            }
            {
                // depth === 1 ?
                //     <text
                //         x={x + 4}
                //         y={y + 18}
                //         fill="#fff"
                //         fontSize={16}
                //         fillOpacity={0.9}
                //     >
                //         {index + 1}
                //     </text>
                //     : null
            }
        </g>
    );
};

export default CustomizedContent;
