import React from 'react';
import { styles } from './styles';


const Tooltip = (props) => {
    if (props.isEnabled) {
        return (
            <div
                {...styles()}
                className="react-chartsjs__tooltip"
                style={{ left: props.x, top: props.y, zIndex: 100 }}
            >
                {props.children}
            </div>
        );
    }
    return null;
};

export default Tooltip;

