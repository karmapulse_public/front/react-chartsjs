import { event as currentEvent } from 'd3-selection';

import {
    D3,
    DEFAULT_HEIGHT,
    DEFAULT_WIDTH
} from './defaultValues';


export default (() => {
    let _width;
    let _height;
    let _svg;
    let _columnWidth;
    let _barWidth;
    let _whiskerWidth;
    let _barGap;
    let _whiskerGap;
    let _xScale;
    let _yScale;

    let _widthBarPercentage;
    let _widthWhiskerPercentage;
    const boxPlotData = [];

    this.props = {};
    this.config = {};

    const margin = {
        top: 20,
        right: 10,
        bottom: 20,
        left: 10
    };

    const sortNumber = (a, b) => b - a;

    const boxQuartiles = d => (
        [
            D3.quantile(d, 0.25),
            D3.quantile(d, 0.5),
            D3.quantile(d, 0.75)
        ]
    );

    const colorScale = (key) => {
        const { data, colors, keyName } = this.props;
        const defaultColors = D3.scaleOrdinal(D3.schemeCategory10)
            .domain(Object.keys(data));

        const color = colors.find(item => item[keyName] === key);
        return color ? color.color : defaultColors(key);
    };

    const setScale = () => {
        const { data } = this.props;

        _xScale = D3.scaleBand()
            .domain(Object.keys(data))
            .range([0, _width - margin.left - margin.right]);

        // Obtener max de toda la muestra de los datos, para creación de escala en Y.
        const max = D3.max(Object.keys(data).map(key => D3.max(data[key])));
        _yScale = D3.scaleLinear()
            .domain([0, max])
            .range([_height - margin.top - margin.bottom, 0])
            .nice();
    };

    const setDimension = (setWidth, setHeight, setBarWidth, setWhiskerWidth) => {
        _width = setWidth;
        _height = setHeight;
        _widthBarPercentage = setBarWidth;
        _widthWhiskerPercentage = setWhiskerWidth;

        if (typeof _height !== 'number') {
            console.warn('Invalid height provided.');
            _height = DEFAULT_HEIGHT;
        }

        if (typeof _width !== 'number') {
            console.warn('Invalid width provided.');
            _width = DEFAULT_WIDTH;
        }

        if (typeof _widthBarPercentage !== 'number') {
            console.warn('Invalid height provided.');
            _height = DEFAULT_HEIGHT;
        }

        if (typeof _widthWhiskerPercentage !== 'number') {
            console.warn('Invalid width provided.');
            _width = DEFAULT_WIDTH;
        }

        // Calcular ancho de barras y bigotes externos
        const { data } = this.props;
        const cleanWidth = _width - margin.left - margin.right;
        _columnWidth = cleanWidth / Object.keys(data).length;
        _barWidth = _columnWidth * _widthBarPercentage;
        _whiskerWidth = _columnWidth * _widthWhiskerPercentage;
        _barGap = (_columnWidth - _barWidth) / 2;
        _whiskerGap = (_columnWidth - _whiskerWidth) / 2;

        setScale();
    };

    const prepareData = () => {
        const { data } = this.props;
        // Ordenamiento de datos
        Object.keys(data).forEach((key) => {
            const groupCount = data[key];
            data[key] = groupCount.sort(sortNumber);
        });

        // Preparar datos
        Object.entries(data).forEach(([key, groupCount]) => {
            const record = {};
            const localMin = D3.min(groupCount);
            const localMax = D3.max(groupCount);

            record.key = key;
            record.counts = groupCount;
            record.quartile = boxQuartiles(groupCount);
            record.whiskers = [localMin, localMax];
            record.color = colorScale(key);
            record.plane = {
                x: _xScale(key) + _barGap,
                height: _yScale(record.quartile[2]) - _yScale(record.quartile[0]),
            };

            boxPlotData.push(record);
        });
    };

    // Rendereo de lineas horizontales
    const horizontalLineConfigs = [
        // Top whisker
        {
            x1: datum => _xScale(datum.key) + _whiskerGap,
            y1: datum => _yScale(datum.whiskers[0]),
            x2: datum => (_xScale(datum.key) + _whiskerWidth + _whiskerGap),
            y2: datum => _yScale(datum.whiskers[0])
        },
        // Median line
        {
            x1: datum => _xScale(datum.key) + _barGap,
            y1: datum => _yScale(datum.quartile[1]),
            x2: datum => (_xScale(datum.key) + _barWidth + _barGap),
            y2: datum => _yScale(datum.quartile[1])
        },
        // Bottom whisker
        {
            x1: datum => _xScale(datum.key) + _whiskerGap,
            y1: datum => _yScale(datum.whiskers[1]),
            x2: datum => (_xScale(datum.key) + _whiskerWidth + _whiskerGap),
            y2: datum => _yScale(datum.whiskers[1])
        }
    ];

    const handleMouseOver = (data, index, items) => {
        const background = D3.select(items[index]).select('.background-bar');
        background.attr('fill-opacity', '0.4');
        this.props.onHover(data);
    };

    const handleMouseMove = (data) => {
        this.config.tooltipState({
            tooltipContent: data.key,
            enableTooltip: true,
            tooltipX: currentEvent.pageX,
            tooltipY: currentEvent.pageY,
        });
    };

    const handleMouseOut = (data, index, items) => {
        const background = D3.select(items[index]).select('.background-bar');
        background.attr('fill-opacity', '0');
        this.config.tooltipState({
            enableTooltip: false,
        });
    };

    const handleMouseClick = (data) => {
        this.props.onClick(data);
    };

    const modifyDOM = () => {
        // Move the left axis over 25 pixels, and the top axis over 35 pixels
        const axisYG = _svg.append('g').attr('transform', 'translate(25, 0)');
        const axisXG = _svg.append('g').attr('transform', `translate(25, ${_height - margin.top - margin.bottom})`);

        // Setup the group the box plot elements will render in
        const g = _svg.append('g')
            .attr('transform', 'translate(25, 0)');

        // Draw the boxes of the box plot, filled in white and on top of vertical lines
        const gC = g.selectAll('.column')
            .data(boxPlotData)
            .enter()
            .append('g')
            .on('click', handleMouseClick)
            .on('mousemove', handleMouseMove)
            .on('mouseover', handleMouseOver)
            .on('mouseout', handleMouseOut);

        gC.append('rect')
            .attr('width', _columnWidth)
            .attr('height', _height - margin.bottom - margin.top)
            .attr('x', datum => _xScale(datum.key))
            .attr('class', 'background-bar')
            .attr('fill', '#d8d8d8')
            .attr('fill-opacity', '0');

        // Draw the box plot vertical lines
        gC.append('line')
            .attr('x1', datum => (_xScale(datum.key) + (_barWidth / 2) + _barGap)) // OPTIMIZAR
            .attr('y1', (datum) => {
                const whisker = datum.whiskers[0];
                return _yScale(whisker);
            })
            .attr('x2', datum => (_xScale(datum.key) + (_barWidth / 2) + _barGap))
            .attr('y2', (datum) => {
                const whisker = datum.whiskers[1];
                return _yScale(whisker);
            })
            .attr('cursor', 'pointer')
            .attr('stroke', '#000')
            .attr('stroke-width', 1)
            .attr('fill', 'none');

        gC.append('rect')
            .attr('width', _barWidth)
            .attr('height', datum => datum.plane.height)
            .attr('cursor', 'pointer')
            .attr('x', datum => datum.plane.x)
            .attr('y', datum => _yScale(datum.quartile[0]))
            .attr('fill', datum => datum.color)
            .attr('stroke', '#000')
            .attr('stroke-width', 1);

        horizontalLineConfigs.forEach((lineConfig) => {
            gC.append('line')
                .attr('x1', lineConfig.x1)
                .attr('y1', lineConfig.y1)
                .attr('x2', lineConfig.x2)
                .attr('y2', lineConfig.y2)
                .attr('cursor', 'pointer')
                .attr('stroke', '#000')
                .attr('stroke-width', 1)
                .attr('fill', 'none');
        });

        // Setup a scale on the left
        const axisY = D3.axisLeft(_yScale);
        axisY.ticks(5);
        axisYG.append('g')
            .call(axisY);
        axisYG.select('.domain').remove();
        axisYG.selectAll('.tick line')
            .attr('x2', _width)
            .attr('stroke', '#efefef')
            .attr('stroke-width', 1);
        axisYG.selectAll('.tick text')
            .attr('font-size', 12);

        // Setup a series axis on the top
        const axisX = D3.axisBottom(_xScale);
        axisX.tickValues(Object.keys(this.props.data).filter((item, index, array) => {
            const total = Math.ceil(array.length / 5);
            return (index % total) === 0;
        }));

        axisXG.append('g')
            .call(axisX);
        axisXG.select('.domain').remove();
        axisXG.selectAll('.tick line').remove();
        axisXG.selectAll('.tick text')
            .attr('font-size', 12);
    };

    const create = (props, config) => {
        this.props = props;
        this.config = config;

        const {
            width,
            height,
            barWidth,
            whiskerWidth
        } = this.props;

        setDimension(width, height, barWidth, whiskerWidth);

        // Creación y seteo de svg con las dimensiones reales.
        _svg = D3.select(this.config.domElement).append('svg')
            .attr('width', _width + margin.left + margin.right)
            .attr('height', _height + margin.top + margin.bottom)
            .append('g')
            .attr('width', _width + margin.left + margin.right)
            .attr('height', _height + margin.top + margin.bottom)
            .attr('transform', `translate(${margin.left},${margin.top})`);

        prepareData();
        modifyDOM();
    };

    const update = (props, config) => {
        this.props = props;
        this.config = config;

        const {
            width,
            height,
            barWidth,
            whiskerWidth
        } = this.props;

        setDimension(width, height, barWidth, whiskerWidth);

        // Creación y seteo de svg con las dimensiones reales.
        _svg = D3.select(this.config.domElement).append('svg')
            .attr('width', _width + margin.left + margin.right)
            .attr('height', _height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', `translate(${margin.left},${margin.top})`);

        prepareData();
        modifyDOM();
    };

    const destroy = (node) => {
        D3.selectAll(node).selectAll('*').remove();
        _svg.remove();
    };

    return {
        create,
        update,
        destroy
    };
})();
