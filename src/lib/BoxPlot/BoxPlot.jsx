import React from 'react';
import { propTypes, defaultProps } from './defaultValues';
import d3BoxPlot from './classD3';
import Tooltip from '../Tooltip/Tooltip';

class BoxPlot extends React.Component {
    container = React.createRef(); // eslint-disable-line
    chart = React.createRef(); // eslint-disable-line

    state = {
        tooltipContent: '',
        enableTooltip: this.props.enableTooltip,
        tooltipX: 0,
        tooltipY: 0,
    }

    componentDidMount() {
        d3BoxPlot.create(
            this.props,
            {
                domElement: this.chart.current,
                tooltipState: this.tooltipState
            }
        );
    }

    componentWillUnmount() {
        d3BoxPlot.destroy(this.chart.current);
    }

    tooltipState = (arrs) => {
        this.setState(arrs);
    }

    render() {
        const {
            tooltipContent,
            enableTooltip,
            tooltipX,
            tooltipY
        } = this.state;
        return (
            <div ref={this.container}>
                <div ref={this.chart} />
                <Tooltip x={tooltipX} y={tooltipY} isEnabled={enableTooltip} >
                    <h1>{tooltipContent}</h1>
                </Tooltip>
            </div>
        );
    }
}

BoxPlot.propTypes = propTypes;
BoxPlot.defaultProps = defaultProps;

export default BoxPlot;
