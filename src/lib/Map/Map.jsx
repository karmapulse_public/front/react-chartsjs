import MapGL, { Marker } from 'react-map-gl';
import React, { Component } from 'react';
import WebMercatorViewport from 'viewport-mercator-project';
import Tooltip from '../Tooltip/Tooltip';
import { styles } from './styles';
import { propTypes, defaultProps } from './defaultValues';

const guid = () => {
    const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    return `${s4()}${s4()}-${s4()}`;
};

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                width: props.width,
                height: props.height,
                latitude: props.latitude,
                longitude: props.longitude,
                zoom: props.zoom,
                maxZoom: props.maxZoom
            }
        };
        this.changeBoudingBox = this.changeBoudingBox.bind(this);
        this.onViewportChange = this.onViewportChange.bind(this);
        this.renderButtonIcon = this.renderButtonIcon.bind(this);
        this.onClickGroup = this.onClickGroup.bind(this);
        this.onClickPoint = this.onClickPoint.bind(this);
        this.getMapBounds = this.getMapBounds.bind(this);
    }

    componentDidMount() {
        const { initialBound } = this.props;

        if (initialBound) {
            this.changeBoudingBox(initialBound);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.boundingBox) {
            const { boundingBox } = nextProps;
            this.changeBoudingBox(boundingBox);
        }
    }

    onViewportChange(viewport) {
        const newViewport = {
            ...viewport
        };
        this.props.onViewportChange({ ...newViewport, bounding_box: this.getMapBounds() });
        this.setState({
            viewport: newViewport
        });
    }

    onClickGroup(data) {
        const { positionName } = this.props;
        let newZoom = this.state.viewport.zoom + 1;
        if (newZoom > this.props.maxZoom) {
            newZoom = this.props.maxZoom;
        }
        this.props.onClickGroup({
            bounding_box: this.getMapBounds(),
            zoom: newZoom,
            data
        });

        this.setState({
            viewport: {
                ...this.state.viewport,
                zoom: newZoom,
                latitude: data[positionName][0],
                longitude: data[positionName][1]
            }
        });
    }

    onClickPoint(data) {
        this.props.onClickPoint({
            bounding_box: this.getMapBounds(),
            zoom: this.state.viewport.zoom,
            data
        });
    }

    getMapBounds() {
        const mapGL = this.mapRef.getMap();
        const bounds = mapGL.getBounds();
        const boundingBox = {
            top_left: {
                lat: bounds._ne.lat,
                lon: bounds._sw.lng
            },
            bottom_right: {
                lat: bounds._sw.lat,
                lon: bounds._ne.lng
            }
        };
        return boundingBox;
    }

    changeBoudingBox(newBounding) {
        const { longitude, latitude, zoom } = new WebMercatorViewport(this.state.viewport)
            .fitBounds(newBounding);
        this.setState({
            viewport: {
                ...this.state.viewport,
                latitude,
                longitude,
                zoom
            }
        });
    }

    renderButtonIcon(data, click, icon) {
        const {
            colors,
            keyName,
            valueName,
            tooltip,
            defaultColor
        } = this.props;
        const color = this.props.colorFind(colors, data, keyName, defaultColor);

        return (
            <div
                {...styles()}
                role="button"
                tabIndex={0}
                onKeyPress={() => { click(data); }}
                onClick={() => { click(data); }}
            >
                <Tooltip isEnabled={this.props.enableTooltip}>
                    {tooltip(data)}
                </Tooltip>
                {icon(color.color, { ...data, value: data[valueName] }, this.props.textStyles)}
            </div>
        );
    }

    renderMarkers() {
        const { valueName, positionName } = this.props;
        const Markers = this.props.data.map((m) => {
            const markerType = m[valueName] > 1 ?
                this.renderButtonIcon(m, this.onClickGroup, this.props.groupIcon)
                : this.renderButtonIcon(m, this.onClickPoint, this.props.singleIcon);
            return (
                <Marker
                    latitude={m[positionName][0]}
                    longitude={m[positionName][1]}
                    offsetLeft={-20}
                    offsetTop={-10}
                    key={guid()}
                    captureClick
                >
                    {markerType}
                </Marker>
            );
        });

        return Markers;
    }

    render() {
        return (
            <MapGL
                {...this.state.viewport}
                ref={(map) => { this.mapRef = map; }}
                mapboxApiAccessToken={this.props.token}
                onViewportChange={this.onViewportChange}
                mapStyle={this.props.mapStyle}
            >
                {this.renderMarkers()}
            </MapGL>
        );
    }
}

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default Map;
