import css from 'glamor-jss';

export const styles = () => css({
    position: 'relative',
    '& .react-chartsjs__tooltip': {
        left: '50%',
        bottom: '110%',
        opacity: 0,
        visibility: 'hidden',
        transition: 'all 0.25s ease',
        transform: 'translate(-50%, 0)',
    },
    '&:hover .react-chartsjs__tooltip': {
        opacity: 1,
        visibility: 'visible',
    }
});

export default styles;
