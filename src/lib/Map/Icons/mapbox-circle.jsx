import React from 'react';
import PropTypes from 'prop-types';
import lightenDarkenColors from './IconFunctions';
import { propTypes, defaultProps } from './defaultValues';

const GroupIcon = props => (
    <svg
        width={props.width}
        height={props.width}
        viewBox="-2 -2 44 44"
        version="1.1"
        xmlnsXlink="http://www.w3.org/1999/xlink"
    >
        <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
            <circle
                cx={20}
                cy={20}
                r={20}
                fill={props.color}
                stroke={lightenDarkenColors(props.color, -30)}
                strokeWidth={2}
            />
            <text {...props.textStyles}>
                <tspan x="46%" y="55%">
                    {props.value}
                </tspan>
            </text>
            <polygon points="0 0 40 0 40 40 0 40" />
        </g>
    </svg>
);

GroupIcon.propTypes = {
    ...propTypes,
    radius: PropTypes.number
};
GroupIcon.defaultProps = {
    ...defaultProps,
    width: 50,
};

export default GroupIcon;
