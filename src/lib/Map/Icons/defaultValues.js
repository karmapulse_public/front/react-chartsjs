import PropTypes from 'prop-types';

export const propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    color: PropTypes.string.isRequired,
    value: PropTypes.number,
    textStyles: PropTypes.object.isRequired,
};

export const defaultProps = {
    value: 0,
    width: 35,
    height: 46,
};
