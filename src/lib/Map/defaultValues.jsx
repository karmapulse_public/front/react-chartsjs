import React from 'react';
import PropTypes from 'prop-types';
import LocationIcon from './Icons/mapbox-marker';
import CircleIcon from './Icons/mapbox-circle';

export const propTypes = {
    token: PropTypes.string.isRequired,
    keyName: PropTypes.string,
    valueName: PropTypes.string,
    positionName: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    initialBound: PropTypes.array,
    zoom: PropTypes.number,
    maxZoom: PropTypes.number,
    colors: PropTypes.array, // key, color
    data: PropTypes.array, // key, value, position
    singleIcon: PropTypes.func,
    groupIcon: PropTypes.func,
    onClickPoint: PropTypes.func,
    onClickGroup: PropTypes.func,
    onViewportChange: PropTypes.func,
    textStyles: PropTypes.object,
    enableTooltip: PropTypes.bool,
    mapStyle: PropTypes.array,
    boundingBox: PropTypes.array,
    defaultColor: PropTypes.string,
    colorFind: PropTypes.func,
};

export const defaultProps = {
    keyName: 'key',
    valueName: 'value',
    positionName: 'position',
    width: 900,
    height: 900,
    latitude: 19.4174043,
    longitude: -99.15973880000001,
    zoom: 15,
    maxZoom: 20,
    data: [],
    colors: [],
    singleIcon: (color, data, styles) =>
        <LocationIcon color={color} textStyles={styles} {...data} />,
    groupIcon: (color, data, styles) =>
        <CircleIcon color={color} textStyles={styles} {...data} />,
    onClickPoint: () => ({}),
    onClickGroup: () => ({}),
    onViewportChange: () => ({}),
    textStyles: {
        fontFamily: 'Roboto-Regular, Roboto',
        fontSize: 10,
        fontWeight: 'normal',
        fill: '#FFF',
        textAnchor: 'middle'
    },
    enableTooltip: false,
    tooltip: data => <p>{data.value}</p>,
    initialBound: null,
    boundingBox: null,
    defaultColor: '#444444',
    colorFind: (colors, data, keyName, defaultColor) => (
        colors.find(item => item[keyName] === data[keyName])
        || { key: data[keyName], color: defaultColor }
    )
};
